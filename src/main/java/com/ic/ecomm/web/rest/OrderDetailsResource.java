package com.ic.ecomm.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.ic.ecomm.config.Constants;
import com.ic.ecomm.domain.OrderDetails;
import com.ic.ecomm.repository.OrderDetailsRepository;
import com.ic.ecomm.security.jwt.TokenProvider;
import com.ic.ecomm.web.rest.errors.BadRequestAlertException;
import com.ic.ecomm.web.rest.util.HeaderUtil;

import io.github.jhipster.web.util.ResponseUtil;
import io.jsonwebtoken.Claims;

/**
 * REST controller for managing OrderDetails.
 */
@RestController
@RequestMapping("/api")
public class OrderDetailsResource {

    private final Logger log = LoggerFactory.getLogger(OrderDetailsResource.class);

    private static final String ENTITY_NAME = "orderDetails";

    private final OrderDetailsRepository orderDetailsRepository;
    
    private final TokenProvider tokenProvider;
    

    public OrderDetailsResource(TokenProvider tokenProvider,OrderDetailsRepository orderDetailsRepository) {
        this.orderDetailsRepository = orderDetailsRepository;
        this.tokenProvider = tokenProvider;
    }

    /**
     * POST  /order-details : Create a new orderDetails.
     *
     * @param orderDetails the orderDetails to create
     * @return the ResponseEntity with status 201 (Created) and with body the new orderDetails, or with status 400 (Bad Request) if the orderDetails has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/order-details")
    @Timed
    public ResponseEntity<OrderDetails> createOrderDetails(@RequestBody OrderDetails orderDetails) throws URISyntaxException {
        log.debug("REST request to save OrderDetails : {}", orderDetails);
        if (orderDetails.getId() != null) {
            throw new BadRequestAlertException("A new orderDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderDetails result = orderDetailsRepository.save(orderDetails);
        return ResponseEntity.created(new URI("/api/order-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /order-details : Updates an existing orderDetails.
     *
     * @param orderDetails the orderDetails to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated orderDetails,
     * or with status 400 (Bad Request) if the orderDetails is not valid,
     * or with status 500 (Internal Server Error) if the orderDetails couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/order-details")
    @Timed
    public ResponseEntity<OrderDetails> updateOrderDetails(@RequestHeader (name="Authorization") String token,@RequestBody OrderDetails orderDetails) throws URISyntaxException {
        log.debug("REST request to update OrderDetails : {}", orderDetails);
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
        	token =  token.substring(7, token.length());
        }
        Claims claims = tokenProvider.getClaims(token);
        String tenantId = claims.get(Constants.TENANT_ID).toString();
        String vendorId = claims.get(Constants.VENDOR_ID).toString();
        
        if (orderDetails.getId() == null) {
            //throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        	//  orderDetailsRepository.updateOrderDetails(orderDetails.getFullOrderStatus().name(),tenantId,vendorId,orderDetails.getOrderNumber());
        	  List<OrderDetails>  orderDetailsList = orderDetailsRepository.findByTenantAndVendorAndOrderNumber(tenantId,vendorId,orderDetails.getOrderNumber());
        	  // @formatter:off
        	OrderDetails result=null;
			for (OrderDetails orderDetailsFronDB : orderDetailsList) {
				orderDetailsFronDB.setFullOrderStatus(orderDetails.getFullOrderStatus());
				result = orderDetailsRepository.save(orderDetailsFronDB);
			} 
			// @formatter:on

             return ResponseEntity.ok()
                 .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
                 .body(result);
             
        }else {
        	 OrderDetails result = orderDetailsRepository.save(orderDetails);
             return ResponseEntity.ok()
                 .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, orderDetails.getId().toString()))
                 .body(result);
        }
       
    }

    /**
     * GET  /order-details : get all the orderDetails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of orderDetails in body
     */
    @GetMapping("/order-details")
    @Timed
    public List<OrderDetails> getAllOrderDetails(@RequestHeader (name="Authorization") String token) {
        log.debug("REST request to get all OrderDetails");
        
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
        	token =  token.substring(7, token.length());
        }
        Claims claims = tokenProvider.getClaims(token);
        String tenantId = claims.get(Constants.TENANT_ID).toString();
        String vendorId = claims.get(Constants.VENDOR_ID).toString();
        
        
        
        return orderDetailsRepository.findByTenantAndVendor(tenantId, vendorId);
    }
    
    /**
     * GET  /order-details : get all the orderDetails.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of orderDetails in body
     */
    @GetMapping("/order-details/search")
    @Timed
    public List<OrderDetails> getAllOrderDetailsByOrderNumber(@RequestHeader (name="Authorization") String token,@RequestParam(required = false, value = "orderNumber") String orderNumber,@RequestParam(required = false, value = "status") String status){
        log.debug("REST request to get all OrderDetails");
        
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
        	token =  token.substring(7, token.length());
        }
        Claims claims = tokenProvider.getClaims(token);
        String tenantId = claims.get(Constants.TENANT_ID).toString();
        String vendorId = claims.get(Constants.VENDOR_ID).toString();
        System.out.println("****************** >>tenantId>>   "+tenantId);
        System.out.println("****************** >>vendorId>>   "+vendorId);
        System.out.println("****************** >>status>>   "+status);
        System.out.println("****************** >>orderNumber>>   "+orderNumber);
        if(status != null && !"".equals(status.trim()) && !"undefined".equals(status.trim())) {
        	 return orderDetailsRepository.findByTenantAndVendorAndStatus(tenantId, vendorId, status);
        }else {
        	 return orderDetailsRepository.findByTenantAndVendorAndOrderNumber(tenantId, vendorId,orderNumber);
        }
        
       
    }

    /**
     * GET  /order-details/:id : get the "id" orderDetails.
     *
     * @param id the id of the orderDetails to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the orderDetails, or with status 404 (Not Found)
     */
    @GetMapping("/order-details/{id}")
    @Timed
    public ResponseEntity<OrderDetails> getOrderDetails(@PathVariable UUID id) {
        log.debug("REST request to get OrderDetails : {}", id);
        Optional<OrderDetails> orderDetails = orderDetailsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orderDetails);
    }

    /**
     * DELETE  /order-details/:id : delete the "id" orderDetails.
     *
     * @param id the id of the orderDetails to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/order-details/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrderDetails(@PathVariable UUID id) {
        log.debug("REST request to delete OrderDetails : {}", id);

        orderDetailsRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
