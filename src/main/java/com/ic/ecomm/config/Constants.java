package com.ic.ecomm.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String DELIMITER ="###";
    public static final String ROLE_USER ="ROLE_USER";

	public static final String TENANT_ID = "TENANT_ID";

	public static final String VENDOR_ID = "VENDOR_ID";
    
    private Constants() {
    }
}
