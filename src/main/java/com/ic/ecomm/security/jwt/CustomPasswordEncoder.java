package com.ic.ecomm.security.jwt;

import java.util.UUID;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.haulmont.cuba.core.global.PasswordEncryption;
import com.haulmont.cuba.core.sys.PasswordEncryptionImpl;
import com.haulmont.cuba.core.sys.encryption.EncryptionModule;
import com.haulmont.cuba.core.sys.encryption.Sha1EncryptionModule;
import com.ic.ecomm.config.Constants;


public class CustomPasswordEncoder implements PasswordEncoder {

	
	 private EncryptionModule encryptionModule = new Sha1EncryptionModule();
	 
	 
	public EncryptionModule getEncryptionModule() {
		return encryptionModule;
	}

	public void setEncryptionModule(EncryptionModule encryptionModule) {
		this.encryptionModule = encryptionModule;
	}

	@Override
	public String encode(CharSequence rawPassword) {
		// TODO Auto-generated method stub
		return encryptionModule.getPlainHash((String) rawPassword);
		
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		// TODO Auto-generated method stub
		boolean result = false;
		int index = encodedPassword.toString().indexOf(Constants.DELIMITER);
		if( index != -1) {
		
			String passwordInDB = encodedPassword.toString().substring(0, index);
			String uuid =  encodedPassword.toString().substring( index+ Constants.DELIMITER.length());
			String claculatedHash = encryptionModule.getPasswordHash(UUID.fromString(uuid), rawPassword.toString());
			result = claculatedHash.equals(passwordInDB);
			
		}{
			
		}
		return result;
		//return false;
	}
	public static void main(String[] args) {
		CustomPasswordEncoder customPasswordEncoder = new CustomPasswordEncoder();
		PasswordEncryptionImpl shaEncrption = new PasswordEncryptionImpl();
		shaEncrption.setEncryptionModule(new Sha1EncryptionModule());
		
		//System.out.println(customPasswordEncoder.encryptionModule.getPasswordHash(UUID.fromString("60885987-1b61-4247-94c7-dff348347f93"),  "admin"));
		
		
		System.out.println(shaEncrption.getPasswordHash(UUID.fromString("3e1712ac-97f6-66e6-ab8d-5bf39992ac50"), "bmpl1"));

	}

}
