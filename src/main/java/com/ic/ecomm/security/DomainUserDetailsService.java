package com.ic.ecomm.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ic.ecomm.config.Constants;
import com.ic.ecomm.domain.CubaUser;
import com.ic.ecomm.domain.User;
import com.ic.ecomm.repository.CubaUserRepository;
import com.ic.ecomm.repository.UserRepository;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);

    private final UserRepository userRepository;
    private final CubaUserRepository cubaUserRepository;

    public DomainUserDetailsService(UserRepository userRepository,CubaUserRepository cubaUserRepository) {
        this.userRepository = userRepository;
        this.cubaUserRepository = cubaUserRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);

        /*
        if (new EmailValidator().isValid(login, null)) {
            Optional<User> userByEmailFromDatabase = userRepository.findOneWithAuthoritiesByEmail(login);
            return userByEmailFromDatabase.map(user -> createSpringSecurityUser(login, user))
                .orElseThrow(() -> new UsernameNotFoundException("User with email " + login + " was not found in the database"));
        }
		*/
        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        /*
        Optional<User> userByLoginFromDatabase = userRepository.findOneWithAuthoritiesByLogin(lowercaseLogin);
        return userByLoginFromDatabase.map(user -> createSpringSecurityUser(lowercaseLogin, user))
            .orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the database"));
        
        */
        Optional<CubaUser> userByLoginFromDatabase = cubaUserRepository.findOneByLogin(lowercaseLogin);
        
        UserDetails userDetails = createSpringSecurityUserCuba(lowercaseLogin, userByLoginFromDatabase.get());
        
        return userByLoginFromDatabase.map(user -> createSpringSecurityUserCuba(lowercaseLogin, user))
            .orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the database"));
        

    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(String lowercaseLogin, User user) {
        if (!user.getActivated()) {
            throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
        }
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
            .map(authority -> new SimpleGrantedAuthority(authority.getName()))
            .collect(Collectors.toList());
        org.springframework.security.core.userdetails.User springUser = new org.springframework.security.core.userdetails.User(user.getLogin(),
                user.getPassword(),
                grantedAuthorities);
        
        return new org.springframework.security.core.userdetails.User(user.getLogin(),
            user.getPassword(),
            grantedAuthorities);
    }
    
    private org.springframework.security.core.userdetails.User createSpringSecurityUserCuba(String lowercaseLogin, CubaUser user) {
        if (!user.getActivated()) {
            throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
        }
        
        // Get the roles here.
        
        List<GrantedAuthority> grantedAuthorities = null;
        if( user.getAuthorities() != null &&  user.getAuthorities().size() > 0) {
        	 grantedAuthorities = user.getAuthorities().stream()
        	            .map(authority -> new SimpleGrantedAuthority(authority.getName()))
        	            .collect(Collectors.toList());
        }else {
        	grantedAuthorities = new ArrayList<GrantedAuthority>();
        	grantedAuthorities.add(new SimpleGrantedAuthority(Constants.ROLE_USER));
        }
       
        return new org.springframework.security.core.userdetails.User(user.getLogin(),
            user.getPassword()+Constants.DELIMITER+user.getId().toString(),
            grantedAuthorities);
    }
}
