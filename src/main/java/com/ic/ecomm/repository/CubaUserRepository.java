package com.ic.ecomm.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.CubaUser;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface CubaUserRepository extends JpaRepository<CubaUser, UUID> {

 
    Optional<CubaUser> findOneByLogin(String login);

}
