package com.ic.ecomm.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.Vendor;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface VendorRepository extends JpaRepository<Vendor, UUID> {

	@Query(value = "select id,vendor_id,client_id  from ecommerceadmin_vendor where vendor_name in (select name from sec_group where id in (select group_id from sec_user where login= ?1))", nativeQuery = true)
    Optional<Vendor> findOneByLogin(String login);

}
