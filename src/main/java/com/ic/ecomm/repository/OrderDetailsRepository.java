package com.ic.ecomm.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ic.ecomm.domain.OrderDetails;


/**
 * Spring Data  repository for the OrderDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, UUID> {
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_id=?2", nativeQuery = true)
	 List<OrderDetails> findByTenantAndVendor(String tenantId, String vendorId);
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_id=?2  and order_number=?3", nativeQuery = true)
	 List<OrderDetails> findByTenantAndVendorAndOrderNumber(String tenantId, String vendorId,String orderNumber);
	
	@Query(value = "select * from ecommerceadmin_order_details where client_id=?1 and vendor_id=?2  and order_status=?3", nativeQuery = true)
	 List<OrderDetails> findByTenantAndVendorAndStatus(String tenantId, String vendorId,String status);
	
	
	@Query(value = "UPDATE ecommerceadmin_order_details set full_order_status =?1 where client_id=?2 and vendor_id=?3  and order_number=?4", nativeQuery = true)
    void updateOrderDetails(String status,String tenantId, String vendorId,String orderNumber);
	
	

}
