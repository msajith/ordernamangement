package com.ic.ecomm.domain;


import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

import com.ic.ecomm.domain.enumeration.OrderStatus;

/**
 * A OrderDetails.
 */
@Entity
@Table(name = "ecommerceadmin_order_details")
public class OrderDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
  	@GenericGenerator(name="system-uuid", strategy = "uuid")
  	@Column(name = "id")
  	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID  id;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "tracking_id")
    private String trackingId;

    @Column(name = "tracking_url")
    private String trackingUrl;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "email")
    private String email;

    @Column(name = "reset_token")
    private String resetToken;

    @Column(name = "time_stamp")
    private Instant createdOn;

    @Column(name = "order_detail_id")
    private Long orderdetailId;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price", precision = 10, scale = 2)
    private BigDecimal price;

    @Column(name = "shipped_date")
    private LocalDate shippedDate;

    @Column(name = "shipping_method")
    private String shippingMethod;

    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "order_dates")
    private String orderDates;

    @Enumerated(EnumType.STRING)
    @Column(name = "full_order_status")
    private OrderStatus fullOrderStatus;

    @Column(name = "customer_address")
    private String customerAddress;

    @Column(name = "sub_total", precision = 10, scale = 2)
    private BigDecimal subTotal;

    @Column(name = "updated_at")
    private Instant updatedOn;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "vendor_id")
    private String vendorId;

    @Enumerated(EnumType.STRING)
    @Column(name = "order_status")
    private OrderStatus orderStatus;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "is_progressed")
    private Boolean isProgressed;

    @Column(name = "is_shipped")
    private Boolean isShipped;

    @Column(name = "delivered_date")
    private LocalDate deliveredDate;
    
    
    @Column(name = "tax", precision = 10, scale = 2)
    private BigDecimal tax;
    
    
    @Column(name = "shipment_fee", precision = 10, scale = 2)
    private BigDecimal shipmentFee;
    
    @Column(name = "total_amount", precision = 10, scale = 2)
    private BigDecimal totalAmount;
    
    @Column(name = "payment_date")
    private LocalDate paymentDate;
    
    
    
    public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getShipmentFee() {
		return shipmentFee;
	}

	public void setShipmentFee(BigDecimal shipmentFee) {
		this.shipmentFee = shipmentFee;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Boolean getIsProgressed() {
		return isProgressed;
	}

	public Boolean getIsShipped() {
		return isShipped;
	}

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public OrderDetails orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getProductName() {
        return productName;
    }

    public OrderDetails productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public OrderDetails trackingId(String trackingId) {
        this.trackingId = trackingId;
        return this;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getTrackingUrl() {
        return trackingUrl;
    }

    public OrderDetails trackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
        return this;
    }

    public void setTrackingUrl(String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public OrderDetails clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getEmail() {
        return email;
    }

    public OrderDetails email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResetToken() {
        return resetToken;
    }

    public OrderDetails resetToken(String resetToken) {
        this.resetToken = resetToken;
        return this;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public OrderDetails createdOn(Instant createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Long getOrderdetailId() {
        return orderdetailId;
    }

    public OrderDetails orderdetailId(Long orderdetailId) {
        this.orderdetailId = orderdetailId;
        return this;
    }

    public void setOrderdetailId(Long orderdetailId) {
        this.orderdetailId = orderdetailId;
    }

    public String getProductId() {
        return productId;
    }

    public OrderDetails productId(String productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public OrderDetails quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public OrderDetails price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getShippedDate() {
        return shippedDate;
    }

    public OrderDetails shippedDate(LocalDate shippedDate) {
        this.shippedDate = shippedDate;
        return this;
    }

    public void setShippedDate(LocalDate shippedDate) {
        this.shippedDate = shippedDate;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public OrderDetails shippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
        return this;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public OrderDetails paymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getOrderDates() {
        return orderDates;
    }

    public OrderDetails orderDates(String orderDates) {
        this.orderDates = orderDates;
        return this;
    }

    public void setOrderDates(String orderDates) {
        this.orderDates = orderDates;
    }

    public OrderStatus getFullOrderStatus() {
        return fullOrderStatus;
    }

    public OrderDetails fullOrderStatus(OrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
        return this;
    }

    public void setFullOrderStatus(OrderStatus fullOrderStatus) {
        this.fullOrderStatus = fullOrderStatus;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public OrderDetails customerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
        return this;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public OrderDetails subTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
        return this;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public OrderDetails updatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCustomerId() {
        return customerId;
    }

    public OrderDetails customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public OrderDetails vendorId(String vendorId) {
        this.vendorId = vendorId;
        return this;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public OrderDetails orderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public OrderDetails orderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public Boolean isIsProgressed() {
        return isProgressed;
    }

    public OrderDetails isProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
        return this;
    }

    public void setIsProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
    }

    public Boolean isIsShipped() {
        return isShipped;
    }

    public OrderDetails isShipped(Boolean isShipped) {
        this.isShipped = isShipped;
        return this;
    }

    public void setIsShipped(Boolean isShipped) {
        this.isShipped = isShipped;
    }

    public LocalDate getDeliveredDate() {
        return deliveredDate;
    }

    public OrderDetails deliveredDate(LocalDate deliveredDate) {
        this.deliveredDate = deliveredDate;
        return this;
    }

    public void setDeliveredDate(LocalDate deliveredDate) {
        this.deliveredDate = deliveredDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OrderDetails orderDetails = (OrderDetails) o;
        if (orderDetails.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderDetails.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
            "id=" + getId() +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", productName='" + getProductName() + "'" +
            ", trackingId='" + getTrackingId() + "'" +
            ", trackingUrl='" + getTrackingUrl() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", email='" + getEmail() + "'" +
            ", resetToken='" + getResetToken() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", orderdetailId=" + getOrderdetailId() +
            ", productId='" + getProductId() + "'" +
            ", quantity=" + getQuantity() +
            ", price=" + getPrice() +
            ", shippedDate='" + getShippedDate() + "'" +
            ", shippingMethod='" + getShippingMethod() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", orderDates='" + getOrderDates() + "'" +
            ", fullOrderStatus='" + getFullOrderStatus() + "'" +
            ", customerAddress='" + getCustomerAddress() + "'" +
            ", subTotal=" + getSubTotal() +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", vendorId='" + getVendorId() + "'" +
            ", orderStatus='" + getOrderStatus() + "'" +
            ", orderDate='" + getOrderDate() + "'" +
            ", isProgressed='" + isIsProgressed() + "'" +
            ", isShipped='" + isIsShipped() + "'" +
            ", deliveredDate='" + getDeliveredDate() + "'" +
            "}";
    }
}
