package com.ic.ecomm.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    Created, Accepted, Processing,Shipped, Delivered, Rejected
}
