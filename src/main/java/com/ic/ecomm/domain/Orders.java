package com.ic.ecomm.domain;


import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import com.ic.ecomm.domain.enumeration.OrderStatus;

/**
 * A Orders.
 */
@Entity
@Table(name = "ecommerceadmin_order")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "id")
	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "items")
    private String items;

    @Column(name = "total_price")
    private String totalPrice;



    @Enumerated(EnumType.STRING)
    @Column(name = "order_status")
    private OrderStatus status;

    @Column(name = "is_shipped")
    private Boolean isShipped;

    @Column(name = "is_progressed")
    private Boolean isProgressed;

    @Column(name = "created_at")
    private Instant createdOn;

    @Column(name = "updated_at")
    private Instant updatedOn;

    @Column(name = "shipped_date")
    private String shippedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public Orders orderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerId() {
        return customerId;
    }

    public Orders customerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getItems() {
        return items;
    }

    public Orders items(String items) {
        this.items = items;
        return this;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public Orders totalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

 

    public OrderStatus getStatus() {
        return status;
    }

    public Orders status(OrderStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Boolean isIsShipped() {
        return isShipped;
    }

    public Orders isShipped(Boolean isShipped) {
        this.isShipped = isShipped;
        return this;
    }

    public void setIsShipped(Boolean isShipped) {
        this.isShipped = isShipped;
    }

    public Boolean isIsProgressed() {
        return isProgressed;
    }

    public Orders isProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
        return this;
    }

    public void setIsProgressed(Boolean isProgressed) {
        this.isProgressed = isProgressed;
    }

    public Instant getCreatedOn() {
        return createdOn;
    }

    public Orders createdOn(Instant createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public void setCreatedOn(Instant createdOn) {
        this.createdOn = createdOn;
    }

    public Instant getUpdatedOn() {
        return updatedOn;
    }

    public Orders updatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public void setUpdatedOn(Instant updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getShippedDate() {
        return shippedDate;
    }

    public Orders shippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
        return this;
    }

    public void setShippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Orders orders = (Orders) o;
        if (orders.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orders.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Orders{" +
            "id=" + getId() +
            ", orderNumber='" + getOrderNumber() + "'" +
            ", customerId='" + getCustomerId() + "'" +
            ", items='" + getItems() + "'" +
            ", totalPrice='" + getTotalPrice() + "'" +
            ", status='" + getStatus() + "'" +
            ", isShipped='" + isIsShipped() + "'" +
            ", isProgressed='" + isIsProgressed() + "'" +
            ", createdOn='" + getCreatedOn() + "'" +
            ", updatedOn='" + getUpdatedOn() + "'" +
            ", shippedDate='" + getShippedDate() + "'" +
            "}";
    }
}
