package com.ic.ecomm.domain;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * A user.
 */
@Entity
@Table(name = "ecommerceadmin_vendor")

public class Vendor  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(generator="system-uuid")
   	@GenericGenerator(name="system-uuid", strategy = "uuid")
   	@Column(name = "id")
   	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

   

 
  
    @Column(name = "vendor_id")
    private String vendorId;

  
    @Column(name = "client_id")
    private String tenantId;


	public UUID getId() {
		return id;
	}


	public void setId(UUID id) {
		this.id = id;
	}


	public String getVendorId() {
		return vendorId;
	}


	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}


	public String getTenantId() {
		return tenantId;
	}


	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public String toString() {
		return "Vendor [vendorId=" + vendorId + ", tenantId=" + tenantId + "]";
	}
    
    

    }
