import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { OrdernamangementOrderDetailsModule } from './order-details/order-details.module';
import { OrdernamangementBillModule } from './bill/bill.module';
import { OrdernamangementOrdersModule } from './orders/orders.module';
//import { JhiLanguageService } from 'ng-jhipster';
//import { JhiLanguageHelper } from 'app/core';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        OrdernamangementOrderDetailsModule,
        OrdernamangementBillModule,
        OrdernamangementOrdersModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdernamangementEntityModule {
    /*
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  languageKey >>>>>>>>  "+ languageKey);
         if (languageKey !== undefined) {
           this.languageService.changeLanguage(languageKey);
         }
        });
       }
       */
}
