import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBill } from 'app/shared/model/bill.model';

type EntityResponseType = HttpResponse<IBill>;
type EntityArrayResponseType = HttpResponse<IBill[]>;

@Injectable({ providedIn: 'root' })
export class BillService {
    private resourceUrl = SERVER_API_URL + 'api/bills';

    constructor(private http: HttpClient) {}

    create(bill: IBill): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(bill);
        return this.http
            .post<IBill>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(bill: IBill): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(bill);
        return this.http
            .put<IBill>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IBill>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IBill[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(bill: IBill): IBill {
        const copy: IBill = Object.assign({}, bill, {
            createdOn: bill.createdOn != null && bill.createdOn.isValid() ? bill.createdOn.toJSON() : null,
            updatedOn: bill.updatedOn != null && bill.updatedOn.isValid() ? bill.updatedOn.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
        res.body.updatedOn = res.body.updatedOn != null ? moment(res.body.updatedOn) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((bill: IBill) => {
            bill.createdOn = bill.createdOn != null ? moment(bill.createdOn) : null;
            bill.updatedOn = bill.updatedOn != null ? moment(bill.updatedOn) : null;
        });
        return res;
    }
}
