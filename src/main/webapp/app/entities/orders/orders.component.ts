import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IOrders } from 'app/shared/model/orders.model';
import { Principal } from 'app/core';
import { OrdersService } from './orders.service';
import { OrderDetailsService } from '../order-details/order-details.service';
import { IOrderDetails } from 'app/shared/model/order-details.model';
import { ActivatedRoute } from '@angular/router';
import { OrderDetails, OrderStatus } from '../../shared/model/order-details.model';

@Component({
    selector: 'jhi-orders',
    templateUrl: './orders.component.html'
})
export class OrdersComponent implements OnInit, OnDestroy {
    orders: IOrders[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private orderDetailsService: OrderDetailsService,
        private ordersService: OrdersService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private route: ActivatedRoute
    ) {}

    loadAll() {
        /*
        this.ordersService.query().subscribe(
            (res: HttpResponse<IOrders[]>) => {
                this.orders = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        */
        console.log('!!!!!!!!!! this.route.snapshot.data[status]  >> ' + this.route.snapshot.data['status']);
        if (
            this.route.snapshot.data['status'] !== null &&
            (this.route.snapshot.data['status'] === OrderStatus.DELIVERED ||
                this.route.snapshot.data['status'] === OrderStatus.REJECTED ||
                this.route.snapshot.data['status'] === OrderStatus.ACCEPTED)
        ) {
            this.orderDetailsService.queryByStatus(this.route.snapshot.data['status']).subscribe(
                (res: HttpResponse<IOrderDetails[]>) => {
                    //console.log(res.body);
                    //this.orders = this.ordersService.extractOrders(res.body);
                    this.orders = [];

                    this.ordersService.extractOrders(res.body).forEach((value: IOrders, key: string) => {
                        this.orders.push(value);
                    });
                    // console.log("Orders return from service: "+ this.orders);
                    // Process the response to create a proper Master - Details  relationship.
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        } else {
            this.orderDetailsService.query().subscribe(
                (res: HttpResponse<IOrderDetails[]>) => {
                    //console.log(res.body);
                    //this.orders = this.ordersService.extractOrders(res.body);
                    this.orders = [];

                    this.ordersService.extractOrders(res.body).forEach((value: IOrders, key: string) => {
                        this.orders.push(value);
                    });
                    // console.log("Orders return from service: "+ this.orders);
                    // Process the response to create a proper Master - Details  relationship.
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInOrders();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IOrders) {
        return item.id;
    }

    registerChangeInOrders() {
        this.eventSubscriber = this.eventManager.subscribe('ordersListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
