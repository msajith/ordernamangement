import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrders } from 'app/shared/model/orders.model';
import { OrderDetails, IOrderDetails } from '../../shared/model/order-details.model';
import { QzTrayService } from './print.service';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'jhi-orders-detail',
    templateUrl: './orders-detail.component.html'
})
export class OrdersDetailComponent implements OnInit {
    orders: IOrders;
    orderDetails: IOrderDetails[];
    printerName: string = 'Thermal';

    message: string = 'Snack Bar opened.';
    actionButtonLabel: string = 'Retry';
    action: boolean = true;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';

    rowHeight: number = 40;

    constructor(
        private activatedRoute: ActivatedRoute,
        private printService: QzTrayService,
        private matSnackBar: MatSnackBar,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ orders }) => {
            //this.orders = orders;

            this.orderDetails = orders;
            console.log('Inside the  NOninit !!!!!! >>>>>>>  ' + JSON.stringify(this.orderDetails));
        });
    }

    trackId(index: number, item: IOrderDetails) {
        return item.id;
    }

    previousState() {
        window.history.back();
    }
    handlePrintButtonClick(event: Event) {
        console.log('Click!', event);

        let printerFound: boolean = false;
        this.printService.getPrinters().subscribe((value: string[]) => {
            console.log('-----------------------------------------------------');
            console.log(JSON.stringify(value));
            console.log('******************************************************');

            value.forEach(element => {
                if (element === this.printerName) {
                    printerFound = true;
                }
            });
            console.log('printerFound >> ' + printerFound);
            if (printerFound) {
                this.printReciept();
            } else {
                alert('Printer with name ' + this.printerName + ' not found.!');
            }
        });
    }
    openSnackBar(message: string, action: string) {
        console.log('Called method with test : ' + message);
        // this.jhiAlertService.success('Called method with : ' + message, null, null);
        this.jhiAlertService.addAlert(
            { type: 'success', msg: 'ordernamangementApp.orderDetails.alert', params: { param: 'this is a test message' }, timeout: 5000 },
            []
        );
    }

    printReciept() {
        let rowValue: number = 50;
        var printData: any[] = [];
        let currDate: string = new Date().toLocaleString();

        printData.push('^XA\n');
        printData.push('^FX Name of the company.\n');
        printData.push('^CF0,60\n');
        printData.push('^FO180,' + rowValue + '^FDiConnect101^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^CF0,40\n');
        printData.push('^FO180,' + rowValue + '^FDE-commerce System^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO180,' + rowValue + '^FDIndia, Dublin,UAE^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO180,' + rowValue + '^FDwww.iconect101.com^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO50,' + rowValue + '^GB700,1,3^FS\n');
        rowValue = rowValue + this.rowHeight;

        printData.push('^FX Second Section.\n');
        printData.push('^CF0,30\n');
        printData.push('^FO50,' + rowValue + '^FDCompany : ' + this.orderDetails[0].clientId + '^FS\n');
        printData.push('^FO450,' + rowValue + '^FDDate : ' + currDate.substring(0, currDate.indexOf(',')) + '^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO50,' + rowValue + '^FDVendor  : ' + this.orderDetails[0].vendorId + '^FS\n');
        printData.push('^FO450,' + rowValue + '^FDTime : ' + currDate.substring(currDate.indexOf(',') + 1) + '^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO50,' + rowValue + '^GB700,1,3^FS\n');
        rowValue = rowValue + this.rowHeight;

        printData.push('^FX Third Section. Line items\n');
        printData.push('^CF0,30\n');

        printData.push('^FO50,' + rowValue + '^FD PRODUCT NAME ^FS\n');
        printData.push('^FO425,' + rowValue + '^FD QTY ^FS\n');
        printData.push('^FO500,' + rowValue + '^FD PRICE ^FS\n');
        printData.push('^FO600,' + rowValue + '^FD SUB TOTAL ^FS\n');
        rowValue = rowValue + this.rowHeight;
        printData.push('^FO50,' + rowValue + '^GB700,1,1^FS\n');
        rowValue = rowValue + this.rowHeight;

        this.orderDetails.forEach(element => {
            //split the line with Max 20 characters
            console.log('Elemnet ######### >>   ' + JSON.stringify(element));
            var nameArray: string[] = element.productName.match(/.{1,25}/g);
            console.log('nameArray item>>>>>>>>>:   ' + nameArray);
            var lineItem: string = '';
            var index: number = 0;

            // Add the product Name
            nameArray.forEach(productName => {
                index = index + 1;
                lineItem = lineItem + '^FO50,' + rowValue + '^FD' + productName + '  ^FS\n';
                if (nameArray.length > 1 && index < nameArray.length) {
                    rowValue = rowValue + this.rowHeight;
                }
            });

            // Add the :  Here
            lineItem = lineItem + '^FO400,' + rowValue + '^FD: ^FS\n';

            // Add the  quanity Here
            lineItem = lineItem + '^FO425,' + rowValue + '^FD' + this.orderDetails[0].quantity + ' ^FS\n';
            // Add the price
            lineItem = lineItem + '^FO475,' + rowValue + '^FD X  ^FS\n';

            lineItem = lineItem + '^FO500,' + rowValue + '^FD ' + this.orderDetails[0].price + ' ^FS\n';

            // Add the subtotal
            lineItem = lineItem + '^FO600,' + rowValue + '^FD' + this.orderDetails[0].subTotal + ' ^FS\n';
            console.log('Line item>>>>>>>>>:   ' + lineItem);
            printData.push(lineItem);
            rowValue = rowValue + this.rowHeight + 10;
        });

        printData.push('^FO50,' + rowValue + '^GB700,1,1^FS\n');
        rowValue = rowValue + this.rowHeight;

        printData.push('^FO50,' + rowValue + '^FD  TAX  ^FS\n');
        printData.push('^FO600,' + rowValue + '^FD ' + this.orderDetails[0].tax + ' ^FS\n');

        rowValue = rowValue + this.rowHeight + 10;

        printData.push('^FO50,' + rowValue + '^FD  DELIVERY CHARGES  ^FS\n');
        printData.push('^FO600,' + rowValue + '^FD ' + this.orderDetails[0].shipmentFee + ' ^FS\n');

        rowValue = rowValue + this.rowHeight + 10;

        printData.push('^FO50,' + rowValue + '^GB700,1,1^FS\n');
        rowValue = rowValue + this.rowHeight;

        printData.push('^FO50,' + rowValue + '^FD  TOTAL  ^FS\n');
        printData.push('^FO600,' + rowValue + '^FD ' + this.orderDetails[0].totalAmount + ' ^FS\n');

        rowValue = rowValue + this.rowHeight + 50;

        printData.push('^FO50,' + rowValue + '^B3N,N,100,Y,N^FD' + this.orderDetails[0].orderNumber + '^FS\n' + '\n');

        /*
        printData.push({ type: 'raw', format: 'image', data: 'content/images/logo-jhipster.png', options: { language: 'ZPLII' } });
        printData.push('^FS\n');
        printData.push('^FO50,160^B3N,N,100,Y,N^FD' + this.orderDetails[0].orderNumber + '^FS\n' + '\n');
        printData.push('^FO50,300^ADN,36,20^FD iConnect101 ^FS\n');
        printData.push('^FO50,350^ADN,36,20^FD ECOMMERCE SYSTEM ^FS\n');
        printData.push('^FO50,400^ADN,36,20^FD VERSION  1.0 ^FS\n' + '\n');
        //  printData.push( '^FO50,450^GB400,0,4,^FS');

        
        this.orderDetails.forEach(element => {
            //split the line with Max 20 characters
            this.rowValue = this.rowValue + 50;
            var lineItem: string = '^FO50,' + this.rowValue + '^ADN,36,20^FD ';
            lineItem = lineItem + element.productName + '    ' + element.price;
            lineItem = lineItem + '^FS\n';
            printData.push(lineItem);
        });
        this.rowValue = this.rowValue + 50;
        printData.push('^FO50,' + this.rowValue + '^GB400,0,4,^FS');
        //printData.push( '^FO50,'+rowNum+'^ADN,36,20^ -------------------------------- ^FS\n' + '\n');
        this.rowValue = this.rowValue + 50;
        var totalSTring: string = '^FO50,' + this.rowValue + '^ADN,36,20^FD Total : ' + this.orderDetails[0].totalAmount + '^FS\n';
        printData.push(totalSTring);
        */
        printData.push('^XZ\n');

        this.printService.printData(this.printerName, printData).subscribe((value: any) => {
            console.log('Return value from print data ' + value);
        });
        this.jhiAlertService.addAlert(
            {
                type: 'success',
                msg: 'ordernamangementApp.orderDetails.alert',
                params: { param: 'Printed successfully' },
                timeout: 5000
            },
            []
        );
    }
    /*
    printReciept() {
        //iterate through the array and generate the string needs to be printed in Zebra printer.
        var data = [
            { type: 'raw', format: 'data', options: { language: "escp", dotDensity: 'double' } },
            '\x1B' + '\x40',          // init
            '\x1B' + '\x61' + '\x31', // center align
            'Beverly Hills, CA  90210' + '\x0A',
            '\x0A',                   // line break
            'www.qz.io' + '\x0A',     // text and line break
            '\x0A',                   // line break
            '\x0A',                   // line break
            'May 18, 2016 10:30 AM' + '\x0A',
            '\x0A',                   // line break
            '\x0A',                   // line break    
            '\x0A',
            'Transaction # 123456 Register: 3' + '\x0A',
            '\x0A',
            '\x0A',
            '\x0A',
            '\x1B' + '\x61' + '\x30', // left align
            'Baklava (Qty 4)       9.00' + '\x1B' + '\x74' + '\x13' + '\xAA', //print special char symbol after numeric
            '\x0A',
            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' + '\x0A',       
            '\x1B' + '\x45' + '\x0D', // bold on
            'Here\'s some bold text!',
            '\x1B' + '\x45' + '\x0A', // bold off
            '\x0A' + '\x0A',
            '\x1B' + '\x61' + '\x32', // right align
            '\x1B' + '\x21' + '\x30', // em mode on
            'DRINK ME',
            '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
            '\x0A' + '\x0A',
            '\x1B' + '\x61' + '\x30', // left align
            '------------------------------------------' + '\x0A',
            '\x1B' + '\x4D' + '\x31', // small text
            'EAT ME' + '\x0A',
            '\x1B' + '\x4D' + '\x30', // normal text
            '------------------------------------------' + '\x0A',
            'normal text',
            '\x1B' + '\x61' + '\x30', // left align
            '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A' + '\x0A',
            '\x1B' + '\x69',          // cut paper (old syntax)
         // '\x1D' + '\x56'  + '\x00' // full cut (new syntax)
         // '\x1D' + '\x56'  + '\x30' // full cut (new syntax)
         // '\x1D' + '\x56'  + '\x01' // partial cut (new syntax)
         // '\x1D' + '\x56'  + '\x31' // partial cut (new syntax)
            '\x10' + '\x14' + '\x01' + '\x00' + '\x05',  // Generate Pulse to kick-out cash drawer**
                                                         // **for legacy drawer cable CD-005A.  Research before using.
         ];

       // window.history.back();
       this.printService.printData(this.printerName,data).subscribe((value: any) => {
           console.log("Return value from print data "+value);
           this.jhiAlertService.addAlert({type: 'success', msg: 'ordernamangementApp.orderDetails.alert', params: { param: 'Printed successfully' },timeout: 5000}, []);  
       });

    }
    */
}
