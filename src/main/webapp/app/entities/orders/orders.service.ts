import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOrders, Orders } from '../../shared/model/orders.model';
import { IOrderDetails } from '../../shared/model/order-details.model';

type EntityResponseType = HttpResponse<IOrders>;
type EntityArrayResponseType = HttpResponse<IOrders[]>;

@Injectable({ providedIn: 'root' })
export class OrdersService {
    private resourceUrl = SERVER_API_URL + 'api/orders';
    //iorders: IOrders[];
    orderMap: Map<string, IOrders>;

    constructor(private http: HttpClient) {}

    extractOrders(ordersDetailsList: IOrderDetails[]): Map<string, IOrders> {
        let map = new Map<string, IOrders>();

        ordersDetailsList.forEach(function(ordersDetails) {
            //console.log(" Iterating through orderdetails -- > "+ordersDetails);

            if (map.get(ordersDetails.orderNumber) !== null && map.get(ordersDetails.orderNumber) !== undefined) {
                //console.log(" Order detauls from   map -- > "+JSON.stringify(map.get(ordersDetails.orderNumber)));
                // Add the order details to the order.
                map.get(ordersDetails.orderNumber).totalPrice = map.get(ordersDetails.orderNumber).totalPrice + ordersDetails.price;
                map.get(ordersDetails.orderNumber).items = map.get(ordersDetails.orderNumber).items + 1;
                map.get(ordersDetails.orderNumber).orderDetails.push(ordersDetails);
            } else {
                // Create a new order object and add to the map.
                const orders: IOrders = new Orders();
                orders.orderNumber = ordersDetails.orderNumber;
                orders.customerId = ordersDetails.customerId;
                orders.totalPrice = ordersDetails.price;
                orders.fullOrderStatus = ordersDetails.fullOrderStatus;
                orders.isShipped = ordersDetails.isShipped;
                orders.isProgressed = ordersDetails.isProgressed;
                orders.createdOn = ordersDetails.createdOn;
                orders.updatedOn = ordersDetails.updatedOn;
                orders.shippedDate = ordersDetails.shippedDate;
                orders.customerAddress = ordersDetails.customerAddress;
                orders.orderDate = ordersDetails.orderDate;
                orders.vendorId = ordersDetails.vendorId;
                orders.trackingId = ordersDetails.trackingId;
                orders.trackingUrl = ordersDetails.trackingUrl;
                orders.orderDetails = [];
                orders.orderDetails.push(ordersDetails);
                orders.items = 1;

                map.set(ordersDetails.orderNumber, orders);
            }
        });

        /*
          let orderArray: IOrders[] = [];

          map.forEach((value: IOrders, key: string) => {
            orderArray.push(value);
        });
        */

        // this.iorders = orderArray;
        this.orderMap = map;
        return map;
    }

    getOrder(orderId: string): IOrders {
        if (this.orderMap) {
            return this.orderMap.get(orderId);
        } else {
            return null;
        }
    }
    create(orders: IOrders): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(orders);
        return this.http
            .post<IOrders>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(orders: IOrders): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(orders);
        return this.http
            .put<IOrders>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IOrders>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOrders[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(orders: IOrders): IOrders {
        const copy: IOrders = Object.assign({}, orders, {
            createdOn: orders.createdOn != null && orders.createdOn.isValid() ? orders.createdOn.toJSON() : null,
            updatedOn: orders.updatedOn != null && orders.updatedOn.isValid() ? orders.updatedOn.toJSON() : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
        res.body.updatedOn = res.body.updatedOn != null ? moment(res.body.updatedOn) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((orders: IOrders) => {
            orders.createdOn = orders.createdOn != null ? moment(orders.createdOn) : null;
            orders.updatedOn = orders.updatedOn != null ? moment(orders.updatedOn) : null;
        });
        return res;
    }
}
