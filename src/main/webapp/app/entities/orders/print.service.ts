import { Observable } from 'rxjs';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';

import * as qz from 'qz-tray';
import * as shajs from 'sha.js';
import * as RSVP from 'rsvp';

@Injectable({ providedIn: 'root' })
export class QzTrayService {
    constructor() {
        qz.security.setCertificatePromise(function(resolve, reject) {
            //Preferred method - from server
            //        $.ajax({ url: "assets/signing/digital-certificate.txt", cache: false, dataType: "text" }).then(resolve, reject);

            //Alternate method 1 - anonymous
            //        resolve();

            //Alternate method 2 - direct
            resolve(
                '-----BEGIN CERTIFICATE-----\n' +
                    'MIIECDCCAvCgAwIBAgIUL6v5R0KYDoOa2hYk/0FCEkumKKAwDQYJKoZIhvcNAQEL\n' +
                    'BQAwgZMxCzAJBgNVBAYTAklOMQ8wDQYDVQQIDAZLZXJhbGExDzANBgNVBAcMBkNv\n' +
                    'Y2hpbjEUMBIGA1UECgwLVGVjaGdlbnRzaWExDDAKBgNVBAsMA0lURDEaMBgGA1UE\n' +
                    'AwwRKi50ZWNoZ2VudHNpYS5jb20xIjAgBgkqhkiG9w0BCQEWE2pveUB0ZWNoZ2Vu\n' +
                    'dHNpYS5jb20wIBcNMTkwNjIzMDkxMTI4WhgPMjA1MDEyMTYwOTExMjhaMIGTMQsw\n' +
                    'CQYDVQQGEwJJTjEPMA0GA1UECAwGS2VyYWxhMQ8wDQYDVQQHDAZDb2NoaW4xFDAS\n' +
                    'BgNVBAoMC1RlY2hnZW50c2lhMQwwCgYDVQQLDANJVEQxGjAYBgNVBAMMESoudGVj\n' +
                    'aGdlbnRzaWEuY29tMSIwIAYJKoZIhvcNAQkBFhNqb3lAdGVjaGdlbnRzaWEuY29t\n' +
                    'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwYh5//aSrIf0lSGg/v3Q\n' +
                    'bFoXY8WXm89UGBvIcizuU8KJCF6nlgBDjK0yDyQ+cTw+um+X9s8aXFsuMpna9w7B\n' +
                    'uiTDvgykvZZ38D5x2eIm2Ga9k+Y42+DW4Fa/Y13WJFVIivFk+JVQa2lW5mdEUGDu\n' +
                    'XuwpsjJ5Zzt3RZYze9U+KXrzUpTlgcew4BZ8tFj+5KbCnTkOHOi7HX1NizrK1L8q\n' +
                    'bXqWkOHHHXo4QbN8DuFOHhGRUJy9sLJW27q4G2oW+tn5UhXuyLGCaKLgNHT4W2WY\n' +
                    'qqZnZV+7EhjlFcRexOlIwMHMzYgtqxG//irugIMCk8iwYmzj5uizWDxeb8kpMnGE\n' +
                    '5wIDAQABo1AwTjAdBgNVHQ4EFgQUL9o/lRjsd9rxostr7qVI0Myxbm8wHwYDVR0j\n' +
                    'BBgwFoAUL9o/lRjsd9rxostr7qVI0Myxbm8wDAYDVR0TBAUwAwEB/zANBgkqhkiG\n' +
                    '9w0BAQsFAAOCAQEAPMQnsTLMeov6MnIP0qhgzKwqjfbu7F+dd2tFUJ8z6BNr+JfX\n' +
                    '5sfHBeiL4XFwTGzt5D8J9+2zRdcm5uO7B9idKQY/0FubHvPHuNY+OJIS5dAhibiI\n' +
                    'ViGyTE6PUwII3zlTV6WTK6BMsEHkh9WZs2tamXRjgjwgpS1npH21voxsVpG7qnbi\n' +
                    'fu++OyqboieWaBEoW8xzdb72WOlR+FFXOMw/QZpGj18HhajjxmfznmFaX9c3K/OA\n' +
                    'K3jHR/FD8Q5valEO6JOknU5SWJmOX1nUXF7AtrOLikYtDUqGnCfz6pdhpEukzcZ1\n' +
                    'pi5Ex1wr3h790E3s4JOBDey7gSCMIKHl9d6u4A==\n' +
                    '-----END CERTIFICATE-----'
            );
        });

        var privateKey =
            '-----BEGIN PRIVATE KEY-----\n' +
            'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDBiHn/9pKsh/SV\n' +
            'IaD+/dBsWhdjxZebz1QYG8hyLO5TwokIXqeWAEOMrTIPJD5xPD66b5f2zxpcWy4y\n' +
            'mdr3DsG6JMO+DKS9lnfwPnHZ4ibYZr2T5jjb4NbgVr9jXdYkVUiK8WT4lVBraVbm\n' +
            'Z0RQYO5e7CmyMnlnO3dFljN71T4pevNSlOWBx7DgFny0WP7kpsKdOQ4c6LsdfU2L\n' +
            'OsrUvyptepaQ4ccdejhBs3wO4U4eEZFQnL2wslbburgbahb62flSFe7IsYJoouA0\n' +
            'dPhbZZiqpmdlX7sSGOUVxF7E6UjAwczNiC2rEb/+Ku6AgwKTyLBibOPm6LNYPF5v\n' +
            'ySkycYTnAgMBAAECggEARy7UeLIPpsfmzQduePuQMpo8aiUeK1/ptWhapKwkCMjP\n' +
            'sDVofIKiVdbKR8lz8iBPjjFHeq8YKgobht8SK7bQyI+HM5rg6R60BLhjFdnZodTA\n' +
            'O+r81NrxDdi6UmkMP5bitMAu/EOnLaq0HNC0zr+oTV+dwn5QKwkzSHQDgbOgRo7e\n' +
            'j8cWP+qVlr+OuIfxR7Hm2y5g2nTnbONRZzXhUYOeAPVmuQBe7wzGg1bFSSR1pmkV\n' +
            '8CTL3CdrOQFsX6r/VDf019rkaDZzGpkJlGGPGpwz+KBxxAWxGvNa+M/X2DSOY+B3\n' +
            'XIKU1h8ICt7TzaUOM24CuxmhbkDbVnoU6BIUMrMA+QKBgQD66rF65PpH3rl3yiGg\n' +
            'VWIk4gJk8/8ZRxIDfgoMVr3YzQjK47V2VErBX1Ygpq87jac1WVJJpC285I3FVh+d\n' +
            'h/8r+J5btksD2DjeIMpTI/HhJr2EyOhMD3UStjLDyoh4D+f+t6+8EtK16EmFwXJj\n' +
            'ssKnRrr8ZehEndTUJslTOPbAfQKBgQDFdC39wWFP+FA6PO4U09HLlqVKg6oy7tkp\n' +
            'rO++K166iaDFq/jXEun4baKP8jJ9KRVs/5PRUOi8PjfG4xOe/9hxnToPO0dwA5FE\n' +
            '5Qrb+PsQry7AWHB0bi+S57t8B76o3EdBGjYDwMurnquMBYrGnVlx6Dx4mS5Lf1SW\n' +
            '9BSTAYGcMwKBgQCHhphSLrdYdl5q60x6U2PtU/3DFV8mAhMiu8Mlovxry2MCYzey\n' +
            'MBkolRvqCt3/JqW31QTzLbBepHxGqBvvdHNJBsJR+wZNj043/DIL8hBFci1POUzv\n' +
            'ODId5zdINW68mqrOE9zhkrEo+8wqaLAmVXl57GQ2y7xGw5Ee/cwkHVJ56QKBgHEP\n' +
            'gz4nuqbl0lpmhRTb7OccTW0LUyty5FoesfA8OTOqlqpZPFU6eUpMzKaL0ikkmyYc\n' +
            'efGC2KDhJ2L0p0mFydPUaWhLUy5qfIXseHlYabo19RkiCtJAPXFqy0nLzXo3F33J\n' +
            'mmAvxu9wHV5B1iK/+nVkuyt62q1ULYV6IUXDuQlNAoGBAInWpgbJ+hPwY7NgnrZ9\n' +
            '5UDQM8fACbdb1FhbLsfu/Grr3e3Rni1Jg5EF2U9KJhc+oOT6zOIFFGT85tyINblP\n' +
            'NQio3O9juK9+gX/e/epVhta6hTY+SI0aoeRUW17LCUfq4MpvVj9IBsD66vHxzXB9\n' +
            'ZeCmplmJZ6hcxiJyKdyuxFNX\n' +
            '-----END PRIVATE KEY-----';

        qz.security.setSignaturePromise(function(toSign) {
            return function(resolve, reject) {
                try {
                    var pk = ''; //KEYUTIL.getKey(privateKey);
                    var sig = null; // new KJUR.crypto.Signature({ alg: 'SHA1withRSA' });
                    sig.init(pk);
                    sig.updateString(toSign);
                    var hex = sig.sign();
                    //console.log('DEBUG: \n\n' + stob64(hextorstr(hex)));
                    resolve(hex); //stob64(hextorstr(hex)));
                } catch (err) {
                    console.error(err);
                    reject(err);
                }
            };
        });
    }

    errorHandler(error: any): Observable<any> {
        console.log('error handler');
        return Observable.throw(error);
    }

    // Get list of printers connected
    getPrinters(): Observable<string[]> {
        /*
        console.log(Observable
            .fromPromise(qz.websocket.connect()
                .then(() => qz.printers.find()))
            .map((printers: string[]) => printers) );
        return Observable
            .fromPromise(qz.websocket.connect()
            .then(() => qz.printers.find()))
            .map((printers: string[]) => printers)
            .catch(this.errorHandler);
            */

        console.log(qz.websocket.isActive());
        if (qz.websocket.isActive()) {
            return Observable.fromPromise(qz.printers.find())
                .map((printers: string[]) => printers)
                .catch(this.errorHandler);
        } else {
            return Observable.fromPromise(qz.websocket.connect().then(() => qz.printers.find()))
                .map((printers: string[]) => printers)
                .catch(this.errorHandler);
        }

        /*
           qz.printers.find().then(function(data) {
            var list = '';
            for(var i = 0; i < data.length; i++) {
                list += "&nbsp; " + data[i] + "<br/>";
            }
            return list;
           
        }).catch();
        */
    }

    // Get the SPECIFIC connected printer
    getPrinter(printerName: string): Observable<string> {
        return Observable.fromPromise(qz.websocket.connect().then(() => qz.printers.find(printerName)))
            .map((printer: string) => printer)
            .catch(this.errorHandler);
    }

    // Print data to chosen printer
    printData(printer: string, data: any): Observable<any> {
        // Create a default config for the found printer
        const config = qz.configs.create(printer);
        return Observable.fromPromise(qz.print(config, data))
            .map((anything: any) => anything)
            .catch(this.errorHandler);
    }

    // Disconnect QZ Tray from the browser
    removePrinter(): void {
        qz.websocket.disconnect();
    }
}
