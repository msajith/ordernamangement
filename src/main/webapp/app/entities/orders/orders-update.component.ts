import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IOrders } from 'app/shared/model/orders.model';
import { OrdersService } from './orders.service';
import {OrderDetailsService} from '../order-details/order-details.service'
import {OrderDetails,IOrderDetails} from '../../shared/model/order-details.model'

@Component({
    selector: 'jhi-orders-update',
    templateUrl: './orders-update.component.html'
})
export class OrdersUpdateComponent implements OnInit {
    private _orders: IOrders;
    isSaving: boolean;
    createdOn: string;
    updatedOn: string;
    orderDetails: IOrderDetails[];

    constructor(private ordersService: OrdersService, private ordersDetailsService: OrderDetailsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ orders }) => {
           // this.orders = orders;
           this.orderDetails = orders;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
       // this.orders.createdOn = moment(this.createdOn, DATE_TIME_FORMAT);
        //this.orders.updatedOn = moment(this.updatedOn, DATE_TIME_FORMAT);
        if (this.orderDetails[0].id !== undefined) {
            this.subscribeToSaveResponse(this.ordersDetailsService.updateAll(this.orderDetails[0]));
        }/* 
        else {
            this.subscribeToSaveResponse(this.ordersService.create(this.orders));
        }
        */
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOrders>>) {
        result.subscribe((res: HttpResponse<IOrders>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get orders() {
        return this._orders;
    }

    set orders(orders: IOrders) {
        this._orders = orders;
        this.createdOn = moment(orders.createdOn).format(DATE_TIME_FORMAT);
        this.updatedOn = moment(orders.updatedOn).format(DATE_TIME_FORMAT);
    }
}
