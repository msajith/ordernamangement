import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Orders } from 'app/shared/model/orders.model';
import { OrdersService } from './orders.service';
import { OrdersComponent } from './orders.component';
import { OrdersDetailComponent } from './orders-detail.component';
import { OrdersUpdateComponent } from './orders-update.component';
import { OrdersDeletePopupComponent } from './orders-delete-dialog.component';
import { IOrders } from 'app/shared/model/orders.model';
import { OrderDetailsService } from '../order-details/order-details.service';
import { OrderDetails, OrderStatus, IOrderDetails } from '../../shared/model/order-details.model';

@Injectable({ providedIn: 'root' })
export class OrdersResolve implements Resolve<any> {
    constructor(private service: OrdersService, private orderDetailsService: OrderDetailsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;

        if (id) {
            //return this.service.find(id).pipe(map((orders: HttpResponse<Orders>) => orders.body));
            console.log('id value in route !!!! >>>>>>> ' + id);
            return this.orderDetailsService.queryByOrderNumber(id).pipe(map((orders: HttpResponse<OrderDetails[]>) => orders.body));

            /*
            console.log("Id in the OrdersResolve > "+ id);
            let  currentOrder = this.service.getOrder(id);
            if(currentOrder){
                return currentOrder;
            }else{
                console.log("currentOrder is empty  > "+ id);
                //this.service.query().pipe(map((orderDetails: HttpResponse<OrderDetails>) => orderDetails.body));
                this.orderDetailsService.query().pipe(map((orderDetails: HttpResponse<OrderDetails[]>) => orderDetails.body)).subscribe(response => {
                    console.log("Result of orderDetailsService.query  > "+ id);
                        return this.service.extractOrders(response).get(id);  
                })
                
            }
            */
        }
        return of(new Orders());
    }
}

export const ordersRoute: Routes = [
    {
        path: 'orders',
        component: OrdersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ordersCompleted',
        component: OrdersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title',
            status: OrderStatus.DELIVERED
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ordersRejected',
        component: OrdersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title',
            status: OrderStatus.REJECTED
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'ordersAccepted',
        component: OrdersComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title',
            status: OrderStatus.ACCEPTED
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'orders/:id/view',
        component: OrdersDetailComponent,
        resolve: {
            orders: OrdersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'orders/new',
        component: OrdersUpdateComponent,
        resolve: {
            orders: OrdersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'orders/:id/edit',
        component: OrdersUpdateComponent,
        resolve: {
            orders: OrdersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ordersPopupRoute: Routes = [
    {
        path: 'orders/:id/delete',
        component: OrdersDeletePopupComponent,
        resolve: {
            orders: OrdersResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ordernamangementApp.orders.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
