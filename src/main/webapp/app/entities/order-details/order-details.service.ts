import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IOrderDetails } from 'app/shared/model/order-details.model';

type EntityResponseType = HttpResponse<IOrderDetails>;
type EntityArrayResponseType = HttpResponse<IOrderDetails[]>;

@Injectable({ providedIn: 'root' })
export class OrderDetailsService {
    private resourceUrl = SERVER_API_URL + 'api/order-details';
    private resourceSearchByOrderNumberUrl = SERVER_API_URL + 'api/order-details/search?orderNumber=';
    private resourceSearchByStatusUrl = SERVER_API_URL + 'api/order-details/search?status=';

    constructor(private http: HttpClient) {}

    create(orderDetails: IOrderDetails): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(orderDetails);
        return this.http
            .post<IOrderDetails>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(orderDetails: IOrderDetails): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(orderDetails);
        return this.http
            .put<IOrderDetails>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }
    updateAll(orderDetails: IOrderDetails): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(orderDetails);
        copy.id = null;
        return this.http
            .put<IOrderDetails>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IOrderDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IOrderDetails[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }
    queryByOrderNumber(orderNumber: string, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);

        console.log('In order details service -->>Order Number ****** -->>> ' + orderNumber);
        console.log('In order details service -->> URL ****** -->>> ' + this.resourceSearchByOrderNumberUrl + orderNumber);
        return this.http
            .get<IOrderDetails[]>(this.resourceSearchByOrderNumberUrl + orderNumber, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }
    queryByStatus(status: string, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);

        console.log('In order details service -->>Order Number ****** -->>> ' + status);
        console.log('In order details service -->> URL ****** -->>> ' + this.resourceSearchByStatusUrl + status);
        return this.http
            .get<IOrderDetails[]>(this.resourceSearchByStatusUrl + status, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(orderDetails: IOrderDetails): IOrderDetails {
        const copy: IOrderDetails = Object.assign({}, orderDetails, {
            createdOn: orderDetails.createdOn != null && orderDetails.createdOn.isValid() ? orderDetails.createdOn.toJSON() : null,
            shippedDate:
                orderDetails.shippedDate != null && orderDetails.shippedDate.isValid()
                    ? orderDetails.shippedDate.format(DATE_FORMAT)
                    : null,
            updatedOn: orderDetails.updatedOn != null && orderDetails.updatedOn.isValid() ? orderDetails.updatedOn.toJSON() : null,
            orderDate:
                orderDetails.orderDate != null && orderDetails.orderDate.isValid() ? orderDetails.orderDate.format(DATE_FORMAT) : null,
            deliveredDate:
                orderDetails.deliveredDate != null && orderDetails.deliveredDate.isValid()
                    ? orderDetails.deliveredDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.createdOn = res.body.createdOn != null ? moment(res.body.createdOn) : null;
        res.body.shippedDate = res.body.shippedDate != null ? moment(res.body.shippedDate) : null;
        res.body.updatedOn = res.body.updatedOn != null ? moment(res.body.updatedOn) : null;
        res.body.orderDate = res.body.orderDate != null ? moment(res.body.orderDate) : null;
        res.body.deliveredDate = res.body.deliveredDate != null ? moment(res.body.deliveredDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((orderDetails: IOrderDetails) => {
            orderDetails.createdOn = orderDetails.createdOn != null ? moment(orderDetails.createdOn) : null;
            orderDetails.shippedDate = orderDetails.shippedDate != null ? moment(orderDetails.shippedDate) : null;
            orderDetails.updatedOn = orderDetails.updatedOn != null ? moment(orderDetails.updatedOn) : null;
            orderDetails.orderDate = orderDetails.orderDate != null ? moment(orderDetails.orderDate) : null;
            orderDetails.deliveredDate = orderDetails.deliveredDate != null ? moment(orderDetails.deliveredDate) : null;
        });
        return res;
    }
}
