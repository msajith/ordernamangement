import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IOrderDetails } from 'app/shared/model/order-details.model';
import { OrderDetailsService } from './order-details.service';

@Component({
    selector: 'jhi-order-details-update',
    templateUrl: './order-details-update.component.html'
})
export class OrderDetailsUpdateComponent implements OnInit {
    private _orderDetails: IOrderDetails;
    isSaving: boolean;
    createdOn: string;
    shippedDateDp: any;
    updatedOn: string;
    orderDateDp: any;
    deliveredDateDp: any;

    constructor(private orderDetailsService: OrderDetailsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ orderDetails }) => {
            this.orderDetails = orderDetails;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.orderDetails.createdOn = moment(this.createdOn, DATE_TIME_FORMAT);
        this.orderDetails.updatedOn = moment(this.updatedOn, DATE_TIME_FORMAT);
        if (this.orderDetails.id !== undefined) {
            this.subscribeToSaveResponse(this.orderDetailsService.update(this.orderDetails));
        } else {
            this.subscribeToSaveResponse(this.orderDetailsService.create(this.orderDetails));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IOrderDetails>>) {
        result.subscribe((res: HttpResponse<IOrderDetails>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get orderDetails() {
        return this._orderDetails;
    }

    set orderDetails(orderDetails: IOrderDetails) {
        this._orderDetails = orderDetails;
        this.createdOn = moment(orderDetails.createdOn).format(DATE_TIME_FORMAT);
        this.updatedOn = moment(orderDetails.updatedOn).format(DATE_TIME_FORMAT);
    }
}
