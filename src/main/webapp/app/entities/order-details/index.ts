export * from './order-details.service';
export * from './order-details-update.component';
export * from './order-details-delete-dialog.component';
export * from './order-details-detail.component';
export * from './order-details.component';
export * from './order-details.route';
