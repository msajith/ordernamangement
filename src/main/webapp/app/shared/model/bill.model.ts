import { Moment } from 'moment';

export const enum OrderStatus {
    CREATED = 'CREATED',
    ACCEPTED = 'ACCEPTED',
    PROCESSING = 'PROCESSING',
    DELIVERED = 'DELIVERED',
    REJECTED = 'REJECTED'
}

export interface IBill {
    id?: number;
    orderNumber?: string;
    customerId?: string;
    items?: string;
    totalPrice?: string;
    description?: string;
    status?: OrderStatus;
    isShipped?: boolean;
    isProgressed?: boolean;
    createdOn?: Moment;
    updatedOn?: Moment;
    shippedDate?: string;
}

export class Bill implements IBill {
    constructor(
        public id?: number,
        public orderNumber?: string,
        public customerId?: string,
        public items?: string,
        public totalPrice?: string,
        public description?: string,
        public status?: OrderStatus,
        public isShipped?: boolean,
        public isProgressed?: boolean,
        public createdOn?: Moment,
        public updatedOn?: Moment,
        public shippedDate?: string
    ) {
        this.isShipped = false;
        this.isProgressed = false;
    }
}
