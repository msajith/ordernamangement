import { Moment } from 'moment';
import{IOrderDetails} from 'app/shared/model/order-details.model';
import { OrderStatus } from './order-details.model';



export interface IOrders {
    id?: number;
    orderNumber?: string;
    customerId?: string;
    items?: number;
    totalPrice?: number;
    description?: string;
    fullOrderStatus?: OrderStatus;
    isShipped?: boolean;
    isProgressed?: boolean;
    createdOn?: Moment;
    updatedOn?: Moment;
    shippedDate?: Moment;
    orderDetails?:IOrderDetails[];
    customerAddress?: string;
    orderDate?: Moment;
    vendorId?: string;
    trackingId?: string;
    trackingUrl?: string;
}

export class Orders implements IOrders {
    constructor(
       
        public orderNumber?: string,
        public customerId?: string,
        public totalPrice?: number,
        public fullOrderStatus?: OrderStatus,
        public isShipped?: boolean,
        public isProgressed?: boolean,
        public createdOn?: Moment,
        public updatedOn?: Moment,
        public shippedDate?: Moment,
        public customerAddress?: string,
        public orderDate?: Moment,
        public vendorId?: string,
        public trackingId?: string,
        public trackingUrl?: string,
        public orderDetails?:IOrderDetails[],
        public description?: string,
        public items?: number,
        public id?: number
    ) {
        this.isShipped = false;
        this.isProgressed = false;
    }
}
