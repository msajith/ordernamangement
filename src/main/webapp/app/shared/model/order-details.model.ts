import { Moment } from 'moment';

export const enum OrderStatus {
    CREATED = 'Created',
    ACCEPTED = 'Accepted',
    PROCESSING = 'Processing',
    DELIVERED = 'Delivered',
    REJECTED = 'Rejected'
}

export interface IOrderDetails {
    id?: number;
    orderNumber?: string;
    productName?: string;
    trackingId?: string;
    trackingUrl?: string;
    clientId?: string;
    email?: string;
    resetToken?: string;
    createdOn?: Moment;
    orderdetailId?: number;
    productId?: string;
    quantity?: number;
    price?: number;
    shippedDate?: Moment;
    shippingMethod?: string;
    paymentStatus?: string;
    orderDates?: string;
    fullOrderStatus?: OrderStatus;
    customerAddress?: string;
    subTotal?: number;
    updatedOn?: Moment;
    customerId?: string;
    vendorId?: string;
    orderStatus?: OrderStatus;
    orderDate?: Moment;
    isProgressed?: boolean;
    isShipped?: boolean;
    deliveredDate?: Moment;
    tax?: number;
    shipmentFee?: number;
    totalAmount?: number;
    paymentDate?: Moment;
}

export class OrderDetails implements IOrderDetails {
    constructor(
        public id?: number,
        public orderNumber?: string,
        public productName?: string,
        public trackingId?: string,
        public trackingUrl?: string,
        public clientId?: string,
        public email?: string,
        public resetToken?: string,
        public createdOn?: Moment,
        public orderdetailId?: number,
        public productId?: string,
        public quantity?: number,
        public price?: number,
        public shippedDate?: Moment,
        public shippingMethod?: string,
        public paymentStatus?: string,
        public orderDates?: string,
        public fullOrderStatus?: OrderStatus,
        public customerAddress?: string,
        public subTotal?: number,
        public updatedOn?: Moment,
        public customerId?: string,
        public vendorId?: string,
        public orderStatus?: OrderStatus,
        public orderDate?: Moment,
        public isProgressed?: boolean,
        public isShipped?: boolean,
        public deliveredDate?: Moment,
        public tax?: number,
        public shipmentFee?: number,
        public totalAmount?: number,
        public paymentDate?: Moment
    ) {
        this.isProgressed = false;
        this.isShipped = false;
    }
}
