package com.ic.ecomm.cucumber.stepdefs;

import com.ic.ecomm.OrdernamangementApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = OrdernamangementApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
